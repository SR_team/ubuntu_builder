FROM ubuntu:18.04

RUN apt update && apt install clang python3 lld wget build-essential git -y --force-yes && \
    apt clean && update-alternatives --set cc /usr/bin/clang && update-alternatives --set c++ /usr/bin/clang++

COPY cmake-bin /usr/local

RUN useradd -ms /bin/bash -u 1000 user
USER user
